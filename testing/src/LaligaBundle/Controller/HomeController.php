<?php

namespace LaligaBundle\Controller;


use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

/**
 * Club controller.
 *
 * @Route("/")
 */
class HomeController extends Controller
{
    /**
     * Lists all club entities.
     *
     * @Route("/", name="home_index")
     * @Method("GET")
     */
    public function indexAction()
    {

        return $this->render('LaligaBundle:home:index.html.twig');
    }
}