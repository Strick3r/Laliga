<?php

namespace LaligaBundle\Controller;

use LaligaBundle\Entity\Jugadores;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;use Symfony\Component\HttpFoundation\Request;

/**
 * Jugadore controller.
 *
 * @Route("jugadores")
 */
class JugadoresController extends Controller
{
    /**
     * Lists all jugadore entities.
     *
     * @Route("/", name="jugadores_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $jugadores = $em->getRepository('LaligaBundle:Jugadores')->findAll();

        return $this->render('LaligaBundle:jugadores:index.html.twig', array(
            'jugadores' => $jugadores,
        ));
    }

    /**
     * Creates a new jugadore entity.
     *
     * @Route("/new", name="jugadores_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $jugadores= new Jugadores();
        $form = $this->createForm('LaligaBundle\Form\JugadoresType', $jugadores);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($jugadores);
            $em->flush();

            return $this->redirectToRoute('jugadores_show', array('id' => $jugadores->getId()));
        }

        return $this->render('LaligaBundle:jugadores:new.html.twig', array(
            'jugadore' => $jugadores,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a jugadore entity.
     *
     * @Route("/{id}", name="jugadores_show")
     * @Method("GET")
     */
    public function showAction(Jugadores $jugadores)
    {
        $deleteForm = $this->createDeleteForm($jugadores);

        return $this->render('LaligaBundle:jugadores:show.html.twig', array(
            'jugadore' => $jugadores,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing jugadore entity.
     *
     * @Route("/{id}/edit", name="jugadores_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Jugadores $jugadores)
    {
        $deleteForm = $this->createDeleteForm($jugadores);
        $editForm = $this->createForm('LaligaBundle\Form\JugadoresType', $jugadores);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('jugadores_edit', array('id' => $jugadores->getId()));
        }

        return $this->render('LaligaBundle:jugadores:edit.html.twig', array(
            'jugadore' => $jugadores,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a jugadore entity.
     *
     * @Route("/{id}", name="jugadores_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, Jugadores $jugadores)
    {
        $form = $this->createDeleteForm($jugadores);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($jugadores);
            $em->flush();
        }

        return $this->redirectToRoute('jugadores_index');
    }

    /**
     * Creates a form to delete a jugadore entity.
     *
     * @param Jugadores $jugadores The jugadore entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Jugadores $jugadores)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('jugadores_delete', array('id' => $jugadores->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
