<?php
// src/AppBundle/Menu/Builder.php
namespace LaligaBundle\Menu;

use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

class Builder implements ContainerAwareInterface
{

use ContainerAwareTrait;

public function Menu(FactoryInterface $factory, array $options)
{
$menu = $factory->createItem('root');

$menu->addChild('Crear Jugadores', array('route' => 'homepage'));

$menu->addChild('About Me', array('route' => 'about'));

$menu['About Me']->addChild('Edit profile', array('route' => 'edit_profile'));

return $menu;
}
}