<?php

namespace LaligaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * club
 *
 * @ORM\Table(name="club")
 * @ORM\Entity(repositoryClass="LaligaBundle\Repository\ClubRepository")
 */
class Club
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     *
     * @ORM\Column(type="string", length=50)
     */
    private $nombre;

    /**
     * @ORM\Column(type="string", length=25)
     */
    private $telefono;

    /**
     * @ORM\OneToMany(targetEntity="Jugadores", mappedBy="club")
     */
    private $jugadores;

    /**
     * @ORM\Column(type="boolean")
     */
    private $borrado;

    public function __construct()
    {
        $this->jugadores = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * @param mixed $nombre
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;
    }

    /**
     * @return mixed
     */
    public function getTelefono()
    {
        return $this->telefono;
    }

    /**
     * @param mixed $telefono
     */
    public function setTelefono($telefono)
    {
        $this->telefono = $telefono;
    }

    /**
     * Get telephones
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getJugadores()
    {
        return $this->jugadores;
    }

    /**
     * @param mixed $jugadores
     */
    public function setJugadores(ArrayCollection $jugadores)
    {
        $this->jugadores = $jugadores;
    }

    /**
     * Remove telephones
     *
     * @param \LaligaBundle\Entity\Jugadores $jugadores
     */
    public function borrarJugadores(\LaligaBundle\Entity\Jugadores $jugadores)
    {
        $this->jugadores->removeElement($jugadores);
    }

    /**
     * @return mixed
     */
    public function getBorrado()
    {
        return $this->borrado;
    }

    /**
     * @param mixed $borrado
     */
    public function setBorrado($borrado)
    {
        $this->borrado = $borrado;
    }


}

